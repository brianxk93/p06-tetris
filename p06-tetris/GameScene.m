//
//  GameScene.m
//  p06-tetris
//
//  Created by Brian Kim on 3/11/16.
//  Copyright (c) 2016 bkim35. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    existingPieces = [[NSMutableArray alloc] init];
    for (int i = 0; i < 20; ++i) {
        NSMutableArray *inner = [[NSMutableArray alloc] initWithCapacity:10];
        
        for (int j = 0; j < 10; ++j) {
            SKShapeNode *initNode = [[SKShapeNode alloc] init];
            initNode.fillColor = [SKColor purpleColor];
            initNode.hidden = YES;
            [inner addObject:initNode];
        }
        
        [existingPieces addObject:inner];
    }
    
    self.size = view.bounds.size;
    pieceBroken = NO;
    
    [self initializeGameBoard];
    [self initializeButtons];
    
    currentPieceActive = NO;
    
    [tetrominoTimer invalidate];
    tetrominoTimer = [NSTimer scheduledTimerWithTimeInterval:.5	target:self selector:@selector(moveTetromino) userInfo:nil repeats:YES];
    
    level = 50;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if([node.name isEqualToString:@"left"]){
        [currentBlock runAction:[SKAction moveByX:-blockWidth y:0 duration:0]];
    }
    if([node.name isEqualToString:@"right"]){
        [currentBlock runAction:[SKAction moveByX:blockWidth y:0 duration:0]];
    }
}

- (SKShapeNode *) createTetromino {
    currentBlockOrientation = ZERO;
    CGMutablePathRef tetrominoPath = CGPathCreateMutable();
    
    nextPiece = arc4random_uniform(6);
    while (!((nextPiece == CYAN_STICK) || (nextPiece == YELLOW_SQUARE))) {
        nextPiece = arc4random_uniform(6);
    }
    
    switch (nextPiece) {
        case YELLOW_SQUARE:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, (blockWidth * 2), 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, (blockWidth * 2), (blockHeight * 2));
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, (blockHeight * 2));
            CGPathCloseSubpath(tetrominoPath);
            break;
        case BLUE_L:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, blockHeight * 2);
            CGPathCloseSubpath(tetrominoPath);
            break;
        case ORANGE_L:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, 0);
            CGPathCloseSubpath(tetrominoPath);
            break;
        case CYAN_STICK:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 4, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 4, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, (blockHeight));
            CGPathCloseSubpath(tetrominoPath);
            break;
        case RED_STAIR:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, blockHeight * 1);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight * 1);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight * 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth, blockHeight);
            CGPathCloseSubpath(tetrominoPath);
            break;
        case GREEN_STAIR:
            CGPathMoveToPoint(tetrominoPath, NULL, 0, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, 0);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 2, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth * 3, blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth , blockHeight * 2);
            CGPathAddLineToPoint(tetrominoPath, NULL, blockWidth, blockHeight);
            CGPathAddLineToPoint(tetrominoPath, NULL, 0, blockHeight);
            CGPathCloseSubpath(tetrominoPath);
            break;
            
    }
    
    SKShapeNode *tetromino = [SKShapeNode shapeNodeWithPath:tetrominoPath centered:YES];
    //set tetromino color
    switch (nextPiece) {
        case YELLOW_SQUARE:
            tetromino.fillColor = [SKColor yellowColor];
            break;
        case BLUE_L:
            tetromino.fillColor = [SKColor blueColor];
            break;
        case ORANGE_L:
            tetromino.fillColor = [SKColor orangeColor];
            break;
        case CYAN_STICK:
            tetromino.fillColor = [SKColor cyanColor];
            break;
        case RED_STAIR:
            tetromino.fillColor = [SKColor redColor];
            break;
        case GREEN_STAIR:
            tetromino.fillColor = [SKColor greenColor];
            break;
    }
    
    return tetromino;
}

-(void)moveTetromino {
    if (currentPieceActive) {
        [currentBlock runAction:[SKAction moveByX:0 y:-blockHeight duration:0]];
        if(tetrisGrid.frame.origin.y >= currentBlock.frame.origin.y){
            NSLog(@"minX: %f", CGRectGetMinX(currentBlock.frame));
            NSLog(@"maxX: %f", CGRectGetMaxX(currentBlock.frame));
            NSLog(@"minY: %f", CGRectGetMinY(currentBlock.frame));
            NSLog(@"maxY: %f", CGRectGetMaxY(currentBlock.frame));
            
            currentPieceActive = NO;
            //store currentBlock data and remove it
            CGRect currentBlockFrame = currentBlock.frame;
//            float currentBlockX = currentBlock.position.x;
//            float currentBlockY = currentBlock.position.y;
            float currentBlockX = currentBlock.position.x;
            float currentBlockY = currentBlock.position.y;

            NSLog(@"Current Block X: %f", currentBlockX);
            NSLog(@"Current Block Y: %f", currentBlockY);
            NSLog(@"Current Block Origin X: %f", currentBlockFrame.origin.x);
            NSLog(@"Current Block Origin Y: %f", currentBlockFrame.origin.y);
            NSLog(@"Current Block Width: %f", currentBlockFrame.size.width);
            NSLog(@"Current Block Height: %f", currentBlockFrame.size.height);
            [currentBlock removeFromParent];
            
            //create individual blocks
            CGMutablePathRef unitBlockPath = CGPathCreateMutable();
            
            CGPathMoveToPoint(unitBlockPath, NULL, 0, 0);
            CGPathAddLineToPoint(unitBlockPath, NULL, blockWidth, 0);
            CGPathAddLineToPoint(unitBlockPath, NULL, blockWidth, blockHeight);
            CGPathAddLineToPoint(unitBlockPath, NULL, 0, blockHeight);
            CGPathCloseSubpath(unitBlockPath);
            
            SKShapeNode *unitBlock1 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
            SKShapeNode *unitBlock2 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
            SKShapeNode *unitBlock3 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
            SKShapeNode *unitBlock4 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
            
            switch (nextPiece) {
                case YELLOW_SQUARE:
                {
                    unitBlock1.fillColor = [SKColor yellowColor];
                    unitBlock2.fillColor = [SKColor yellowColor];
                    unitBlock3.fillColor = [SKColor yellowColor];
                    unitBlock4.fillColor = [SKColor yellowColor];
                    
//                    unitBlock1.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                    unitBlock2.position = CGPointMake(currentBlockX + blockWidth + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                    unitBlock3.position = CGPointMake(currentBlockX + blockWidth + (blockWidth / 2.0), currentBlockY + blockHeight + (blockHeight / 2.0));
//                    unitBlock4.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + blockHeight + (blockHeight / 2.0));
                    
                    unitBlock1.position = CGPointMake(currentBlockX - blockWidth, currentBlockY - blockHeight);
                    unitBlock2.position = CGPointMake(currentBlockX, currentBlockY - blockHeight);
                    unitBlock3.position = CGPointMake(currentBlockX - blockWidth, currentBlockY);
                    unitBlock4.position = CGPointMake(currentBlockX, currentBlockY);
                    
//                    int block1x = unitBlock1.position.x / blockWidth;
//                    int block1y = unitBlock1.position.y / blockHeight;
//                    existingPieces[block1x][block1y] = skshapenode;
                    break;
                }
                case BLUE_L:
                    unitBlock1.fillColor = [SKColor blueColor];
                    unitBlock2.fillColor = [SKColor blueColor];
                    unitBlock3.fillColor = [SKColor blueColor];
                    unitBlock4.fillColor = [SKColor blueColor];
                {
                    break;
                }
                case ORANGE_L:
                {
                    unitBlock1.fillColor = [SKColor orangeColor];
                    unitBlock2.fillColor = [SKColor orangeColor];
                    unitBlock3.fillColor = [SKColor orangeColor];
                    unitBlock4.fillColor = [SKColor orangeColor];
                    break;
                }
                case CYAN_STICK:
                {
                    unitBlock1.fillColor = [SKColor cyanColor];
                    unitBlock2.fillColor = [SKColor cyanColor];
                    unitBlock3.fillColor = [SKColor cyanColor];
                    unitBlock4.fillColor = [SKColor cyanColor];
                    
//                    if (currentBlockFrame.size.width < currentBlockFrame.size.height) {
//                        unitBlock1.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight * 0) + (blockHeight / 2.0));
//                        unitBlock2.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight * 1) + (blockHeight / 2.0));
//                        unitBlock3.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight * 2) + (blockHeight / 2.0));
//                        unitBlock4.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight * 3) + (blockHeight / 2.0));
//                    } else {
//                        unitBlock1.position = CGPointMake(currentBlockX + (blockWidth * 0) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                        unitBlock2.position = CGPointMake(currentBlockX + (blockWidth * 1) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                        unitBlock3.position = CGPointMake(currentBlockX + (blockWidth * 2) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                        unitBlock4.position = CGPointMake(currentBlockX + (blockWidth * 3) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                    }
                    
                    if (currentBlockFrame.size.width < currentBlockFrame.size.height) {
                        unitBlock1.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY - (blockHeight * 2));
                        unitBlock2.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY - (blockHeight * 1));
                        unitBlock3.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY + (blockHeight * 0));
                        unitBlock4.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY + (blockHeight * 1));
                    } else {
                        unitBlock1.position = CGPointMake(currentBlockX - (blockWidth * 2), currentBlockY - (blockWidth / 2.0));
                        unitBlock2.position = CGPointMake(currentBlockX - (blockWidth * 1), currentBlockY - (blockWidth / 2.0));
                        unitBlock3.position = CGPointMake(currentBlockX + (blockWidth * 0), currentBlockY - (blockWidth / 2.0));
                        unitBlock4.position = CGPointMake(currentBlockX + (blockWidth * 1), currentBlockY - (blockWidth / 2.0));
                    }
                    
                    break;
                }
                case RED_STAIR:
                {
                    unitBlock1.fillColor = [SKColor redColor];
                    unitBlock2.fillColor = [SKColor redColor];
                    unitBlock3.fillColor = [SKColor redColor];
                    unitBlock4.fillColor = [SKColor redColor];
                    break;
                }
                case GREEN_STAIR:
                {
                    unitBlock1.fillColor = [SKColor greenColor];
                    unitBlock2.fillColor = [SKColor greenColor];
                    unitBlock3.fillColor = [SKColor greenColor];
                    unitBlock4.fillColor = [SKColor greenColor];
                    break;
                }
            }
            
            [self addChild:unitBlock1];
            [self addChild:unitBlock2];
            [self addChild:unitBlock3];
            [self addChild:unitBlock4];
            
            int unitBlock1X = (CGRectGetMidX(unitBlock1.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
            int unitBlock1Y = (CGRectGetMidY(unitBlock1.frame) - tetrisGrid.frame.origin.y) / blockHeight;
            int unitBlock2X = (CGRectGetMidX(unitBlock2.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
            int unitBlock2Y = (CGRectGetMidY(unitBlock2.frame) - tetrisGrid.frame.origin.y) / blockHeight;
            int unitBlock3X = (CGRectGetMidX(unitBlock3.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
            int unitBlock3Y = (CGRectGetMidY(unitBlock3.frame) - tetrisGrid.frame.origin.y) / blockHeight;
            int unitBlock4X = (CGRectGetMidX(unitBlock4.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
            int unitBlock4Y = (CGRectGetMidY(unitBlock4.frame) - tetrisGrid.frame.origin.y) / blockHeight;
            NSLog(@"If statement");
            NSLog(@"Tetris Grid Origin X: %f", tetrisGrid.frame.origin.x);
            NSLog(@"Tetris Grid Origin Y: %f", tetrisGrid.frame.origin.y);
            NSLog(@"Unit Block 1 X: %f", CGRectGetMidX(unitBlock1.frame));
            NSLog(@"Unit Block 1 Y: %f", CGRectGetMidY(unitBlock1.frame));
            
            NSLog(@"Unit Block 2 X: %f", CGRectGetMidX(unitBlock2.frame));
            NSLog(@"Unit Block 2 Y: %f", CGRectGetMidY(unitBlock2.frame));
            
            NSLog(@"Unit Block 3 X: %f", CGRectGetMidX(unitBlock3.frame));
            NSLog(@"Unit Block 3 Y: %f", CGRectGetMidY(unitBlock3.frame));
            
            NSLog(@"Unit Block 4 X: %f", CGRectGetMidX(unitBlock4.frame));
            NSLog(@"Unit Block 4 Y: %f", CGRectGetMidY(unitBlock4.frame));
            
            NSLog(@"unitBlock1x:%d , unitBlock1y:%d", unitBlock1X, unitBlock1Y);
            NSLog(@"unitBlock2x:%d , unitBlock2y:%d", unitBlock2X, unitBlock2Y);
            NSLog(@"unitBlock3x:%d , unitBlock3y:%d", unitBlock3X, unitBlock3Y);
            NSLog(@"unitBlock4x:%d , unitBlock4y:%d", unitBlock4X, unitBlock4Y);
            
            existingPieces[unitBlock1Y][unitBlock1X] = unitBlock1;
            existingPieces[unitBlock2Y][unitBlock2X] = unitBlock2;
            existingPieces[unitBlock3Y][unitBlock3X] = unitBlock3;
            existingPieces[unitBlock4Y][unitBlock4X] = unitBlock4;
            
            
            [self checkIfLineFull];
        } else {
            for (NSMutableArray *row in existingPieces){
                
                if (pieceBroken) {
                    pieceBroken = false;
                    break;
                }
            
                
                for(SKShapeNode *piece in row){
                    if ([currentBlock intersectsNode:piece]) {
                        if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                             fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) ||
                             (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                              fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6)||
                             (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                              fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6)||
                             (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                              fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6)||
                             (fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                              fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6)||
                             (fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                              fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6))){
                                 if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6) || (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6))){
                                     pieceBroken = YES;
                                     
                                     currentPieceActive = NO;
                                     //[currentBlock runAction:[SKAction moveByX:0 y:(blockHeight) duration:0]];
                                     
                                     //store currentBlock data and remove it
                                     CGRect currentBlockFrame = currentBlock.frame;
                                     float currentBlockX = currentBlock.position.x;
                                     float currentBlockY = currentBlock.position.y;
                                     [currentBlock removeFromParent];
                                     
                                     //create individual blocks
                                     CGMutablePathRef unitBlockPath = CGPathCreateMutable();
                                     
                                     CGPathMoveToPoint(unitBlockPath, NULL, 0, 0);
                                     CGPathAddLineToPoint(unitBlockPath, NULL, blockWidth, 0);
                                     CGPathAddLineToPoint(unitBlockPath, NULL, blockWidth, blockHeight);
                                     CGPathAddLineToPoint(unitBlockPath, NULL, 0, blockHeight);
                                     CGPathCloseSubpath(unitBlockPath);
                                     
                                     SKShapeNode *unitBlock1 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
                                     SKShapeNode *unitBlock2 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
                                     SKShapeNode *unitBlock3 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
                                     SKShapeNode *unitBlock4 = [SKShapeNode shapeNodeWithPath:unitBlockPath];
                                     
                                     switch (nextPiece) {
                                         case YELLOW_SQUARE:
                                         {
                                             unitBlock1.fillColor = [SKColor yellowColor];
                                             unitBlock2.fillColor = [SKColor yellowColor];
                                             unitBlock3.fillColor = [SKColor yellowColor];
                                             unitBlock4.fillColor = [SKColor yellowColor];
                                             
//                                             unitBlock1.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                             unitBlock2.position = CGPointMake(currentBlockX + blockWidth + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                             unitBlock3.position = CGPointMake(currentBlockX + blockWidth + (blockWidth / 2.0), currentBlockY + blockHeight + (blockHeight / 2.0));
//                                             unitBlock4.position = CGPointMake(currentBlockX + (blockWidth / 2.0), currentBlockY + blockHeight + (blockHeight / 2.0));
                                             
                                             unitBlock1.position = CGPointMake(currentBlockX - blockWidth, currentBlockY - blockHeight);
                                             unitBlock2.position = CGPointMake(currentBlockX, currentBlockY - blockHeight);
                                             unitBlock3.position = CGPointMake(currentBlockX - blockWidth, currentBlockY);
                                             unitBlock4.position = CGPointMake(currentBlockX, currentBlockY);

                                             break;
                                         }
                                         case BLUE_L:
                                             unitBlock1.fillColor = [SKColor blueColor];
                                             unitBlock2.fillColor = [SKColor blueColor];
                                             unitBlock3.fillColor = [SKColor blueColor];
                                             unitBlock4.fillColor = [SKColor blueColor];
                                         {
                                             break;
                                         }
                                         case ORANGE_L:
                                         {
                                             unitBlock1.fillColor = [SKColor orangeColor];
                                             unitBlock2.fillColor = [SKColor orangeColor];
                                             unitBlock3.fillColor = [SKColor orangeColor];
                                             unitBlock4.fillColor = [SKColor orangeColor];
                                             break;
                                         }
                                         case CYAN_STICK:
                                         {
                                             unitBlock1.fillColor = [SKColor cyanColor];
                                             unitBlock2.fillColor = [SKColor cyanColor];
                                             unitBlock3.fillColor = [SKColor cyanColor];
                                             unitBlock4.fillColor = [SKColor cyanColor];
                                             
//                                             if (currentBlockFrame.size.width < currentBlockFrame.size.height) {
//                                                 unitBlock1.position = CGPointMake(currentBlockX, currentBlockY + (blockHeight * 0) + (blockHeight / 2.0));
//                                                 unitBlock2.position = CGPointMake(currentBlockX, currentBlockY + (blockHeight * 1) + (blockHeight / 2.0));
//                                                 unitBlock3.position = CGPointMake(currentBlockX, currentBlockY + (blockHeight * 2) + (blockHeight / 2.0));
//                                                 unitBlock4.position = CGPointMake(currentBlockX, currentBlockY + (blockHeight * 3) + (blockHeight / 2.0));
//                                             } else {
//                                                 unitBlock1.position = CGPointMake(currentBlockX + (blockWidth * 0) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                                 unitBlock2.position = CGPointMake(currentBlockX + (blockWidth * 1) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                                 unitBlock3.position = CGPointMake(currentBlockX + (blockWidth * 2) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                                 unitBlock4.position = CGPointMake(currentBlockX + (blockWidth * 3) + (blockWidth / 2.0), currentBlockY + (blockHeight / 2.0));
//                                             }
                                             
                                             if (currentBlockFrame.size.width < currentBlockFrame.size.height) {
                                                 unitBlock1.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY - (blockHeight * 2));
                                                 unitBlock2.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY - (blockHeight * 1));
                                                 unitBlock3.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY + (blockHeight * 0));
                                                 unitBlock4.position = CGPointMake(currentBlockX - (blockWidth / 2.0), currentBlockY + (blockHeight * 1));
                                             } else {
                                                 unitBlock1.position = CGPointMake(currentBlockX - (blockWidth * 2), currentBlockY - (blockWidth / 2.0));
                                                 unitBlock2.position = CGPointMake(currentBlockX - (blockWidth * 1), currentBlockY - (blockWidth / 2.0));
                                                 unitBlock3.position = CGPointMake(currentBlockX + (blockWidth * 0), currentBlockY - (blockWidth / 2.0));
                                                 unitBlock4.position = CGPointMake(currentBlockX + (blockWidth * 1), currentBlockY - (blockWidth / 2.0));
                                             }
                                             
                                             break;
                                         }
                                         case RED_STAIR:
                                         {
                                             unitBlock1.fillColor = [SKColor redColor];
                                             unitBlock2.fillColor = [SKColor redColor];
                                             unitBlock3.fillColor = [SKColor redColor];
                                             unitBlock4.fillColor = [SKColor redColor];
                                             break;
                                         }
                                         case GREEN_STAIR:
                                         {
                                             unitBlock1.fillColor = [SKColor greenColor];
                                             unitBlock2.fillColor = [SKColor greenColor];
                                             unitBlock3.fillColor = [SKColor greenColor];
                                             unitBlock4.fillColor = [SKColor greenColor];
                                             break;
                                         }
                                     }
                                     
                                     [self addChild:unitBlock1];
                                     [self addChild:unitBlock2];
                                     [self addChild:unitBlock3];
                                     [self addChild:unitBlock4];
                                     
                                     int unitBlock1X = (CGRectGetMidX(unitBlock1.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
                                     int unitBlock1Y = (CGRectGetMidY(unitBlock1.frame) - tetrisGrid.frame.origin.y) / blockHeight;
                                     int unitBlock2X = (CGRectGetMidX(unitBlock2.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
                                     int unitBlock2Y = (CGRectGetMidY(unitBlock2.frame) - tetrisGrid.frame.origin.y) / blockHeight;
                                     int unitBlock3X = (CGRectGetMidX(unitBlock3.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
                                     int unitBlock3Y = (CGRectGetMidY(unitBlock3.frame) - tetrisGrid.frame.origin.y) / blockHeight;
                                     int unitBlock4X = (CGRectGetMidX(unitBlock4.frame) - tetrisGrid.frame.origin.x * 2) / blockWidth;
                                     int unitBlock4Y = (CGRectGetMidY(unitBlock4.frame) - tetrisGrid.frame.origin.y) / blockHeight;
                                     NSLog(@"Else Statement");
                                     NSLog(@"Tetris Grid Origin X: %f", tetrisGrid.frame.origin.x);
                                     NSLog(@"Tetris Grid Origin Y: %f", tetrisGrid.frame.origin.y);
                                     NSLog(@"Unit Block 1 X: %f", CGRectGetMidX(unitBlock1.frame));
                                     NSLog(@"Unit Block 1 Y: %f", CGRectGetMidY(unitBlock1.frame));
                                     
                                     NSLog(@"Unit Block 2 X: %f", CGRectGetMidX(unitBlock2.frame));
                                     NSLog(@"Unit Block 2 Y: %f", CGRectGetMidY(unitBlock2.frame));
                                     
                                     NSLog(@"Unit Block 3 X: %f", CGRectGetMidX(unitBlock3.frame));
                                     NSLog(@"Unit Block 3 Y: %f", CGRectGetMidY(unitBlock3.frame));
                                                             
                                     NSLog(@"Unit Block 4 X: %f", CGRectGetMidX(unitBlock4.frame));
                                     NSLog(@"Unit Block 4 Y: %f", CGRectGetMidY(unitBlock4.frame));
                                     
                                     NSLog(@"unitBlock1x:%d , unitBlock1y:%d", unitBlock1X, unitBlock1Y);
                                     NSLog(@"unitBlock2x:%d , unitBlock2y:%d", unitBlock2X, unitBlock2Y);
                                     NSLog(@"unitBlock3x:%d , unitBlock3y:%d", unitBlock3X, unitBlock3Y);
                                     NSLog(@"unitBlock4x:%d , unitBlock4y:%d", unitBlock4X, unitBlock4Y);
                                     
                                     existingPieces[unitBlock1Y][unitBlock1X] = unitBlock1;
                                     existingPieces[unitBlock2Y][unitBlock2X] = unitBlock2;
                                     existingPieces[unitBlock3Y][unitBlock3X] = unitBlock3;
                                     existingPieces[unitBlock4Y][unitBlock4X] = unitBlock4;
                                     break;

                            }
                        
                        }
                    }
                }
            }
            
           [self checkIfLineFull];
        }
        
        //check if line cleared
        
        
    } else {
        currentBlock = [self createTetromino];
        
        switch (nextPiece) {
            case YELLOW_SQUARE:
            {
                currentBlock.position = CGPointMake(tetrisGrid.frame.origin.x * 2 + (blockWidth * 5), tetrisGrid.frame.origin.y+(18*blockHeight));
                break;
            }
            case BLUE_L:
            {
                break;
            }
            case ORANGE_L:
            {
                break;
            }
            case CYAN_STICK:
            {
                currentBlock.position = CGPointMake(tetrisGrid.frame.origin.x * 2 + (blockWidth * 5), tetrisGrid.frame.origin.y+(18*blockHeight) + (blockHeight / 2.0));
                break;
            }
            case RED_STAIR:
            {
                break;
            }
            case GREEN_STAIR:
            {
                break;
            }
        }
        
        [self addChild:currentBlock];
        
        currentPieceActive = YES;
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    //[currentBlock runAction:[SKAction moveByX:0 y:-blockHeight duration:0]];
}

- (void) initializeGameBoard{
    gameScreenHeight = CGRectGetHeight(self.scene.view.bounds);
    gameScreenWidth = CGRectGetWidth(self.scene.view.bounds);

    CGRect tetrisFrame;
    tetrisFrame.origin.x = gameScreenWidth / 12.0;
    tetrisFrame.origin.y = (gameScreenHeight / 12.0);
    NSLog(@"tetrisGrid.x: %f", tetrisFrame.origin.x);
    NSLog(@"tetrisGrid.y: %f", tetrisFrame.origin.y);
    tetrisFrame.size.width = gameScreenWidth * (2.0/3.0);
    tetrisFrame.size.height = tetrisFrame.size.width * 2;
    
    tetrisGrid = [[UIView alloc] initWithFrame:tetrisFrame];
  
    //this may be redundant these two lines since all squares have same dimensions
    blockWidth = blockHeight = tetrisGrid.frame.size.width / 10.0;
    
    NSLog(@"Screen height = %f, scree width = %f", gameScreenHeight, gameScreenWidth);
    NSLog(@"height = %f, width = %f", tetrisGrid.frame.size.height, tetrisGrid.frame.size.width);
    NSLog(@"blockheight = %f, blockwidth = %f", blockHeight, blockWidth);
    
    
    //used these variables to determine where to draw the blocks
    float currentX = CGRectGetMinX(tetrisGrid.frame);
    float currentY = CGRectGetMaxY(tetrisGrid.frame);
    
    //create 2D Array
    NSMutableArray *tetrisArray = [NSMutableArray array];
    for(int i = 0; i < 20; i++){
        //blocks from top left point so we have to move up initially
        currentY -= blockHeight;
        NSMutableArray *innerArray = [NSMutableArray array];
        currentX = CGRectGetMinX(tetrisGrid.frame);
        for(int j = 0; j < 10; j++){
            
            
            //PUT BLOCK OBJECTS HERE LATER
            UILabel *block = [[UILabel alloc] initWithFrame:CGRectMake(currentX, currentY, blockWidth, blockHeight)];
            //white 1px border
            block.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:1.0f].CGColor;
            block.layer.borderWidth = 1.0f;
            [tetrisGrid addSubview:block];
            //place block next to it
            currentX += blockWidth;
            
            //setting spawn location
            //if(i == 19 && j == 4){
              //  spawnPoint = CGPointMake(currentX, currentY);
            //}
            
        }
        [tetrisArray addObject:innerArray];
    }
    
    [self.scene.view addSubview:tetrisGrid];
}

- (void) initializeButtons {
    //left button
    UIButton *moveLeftButton = [[UIButton alloc]initWithFrame:CGRectMake(gameScreenWidth / 25.0, gameScreenHeight / 3.0, 50, 50)];
    UIImage *moveLeftImage = [UIImage imageNamed:@"moveLeft.jpg"];
    [moveLeftButton setImage:moveLeftImage forState:UIControlStateNormal];
    [moveLeftButton addTarget:self action:@selector(movePieceLeft) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moveLeftButton];
    
    //right button
    UIButton *moveRightButton = [[UIButton alloc]initWithFrame:CGRectMake(gameScreenWidth / 1.2, gameScreenHeight / 3.0, 50, 50)];
    UIImage *moveRightImage = [UIImage imageNamed:@"moveRight.jpg"];
    [moveRightButton setImage:moveRightImage forState:UIControlStateNormal];
    [moveRightButton addTarget:self action:@selector(movePieceRight) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moveRightButton];
    
    //Rotate Counter Clockwise Button
    UIButton *rotateCounterClockwiseButton = [[UIButton alloc]initWithFrame:CGRectMake(gameScreenWidth / 25.0, gameScreenHeight / 4.0, 50, 50)];
    UIImage *rotateCounterClockwiseImage = [UIImage imageNamed:@"rotateLeft.gif"]; //Using moveLeft.jpg for debug purposes until we get an image for the rotate button
    [rotateCounterClockwiseButton setImage:rotateCounterClockwiseImage forState:UIControlStateNormal];
    [rotateCounterClockwiseButton addTarget:self action:@selector(rotateCounterClockwise) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rotateCounterClockwiseButton];
    
    //Rotate Clockwise Button
    UIButton *rotateClockwiseButton = [[UIButton alloc]initWithFrame:CGRectMake(gameScreenWidth / 1.2, gameScreenHeight / 4.0, 50, 50)];
    UIImage *rotateClockwiseImage = [UIImage imageNamed:@"rotateRight.gif"]; //Using moveRight.jpg for debug purposes until we get an image for the rotate button
    [rotateClockwiseButton setImage:rotateClockwiseImage forState:UIControlStateNormal];
    [rotateClockwiseButton addTarget:self action:@selector(rotateClockwise) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rotateClockwiseButton];
    
}

-(void) movePieceLeft {
    [currentBlock runAction:[SKAction moveByX:-blockWidth y:0 duration:0]];
    if (currentBlock.frame.origin.x <= ((tetrisGrid.frame.origin.x * 2))) {
        [currentBlock runAction:[SKAction moveByX:blockWidth y:0 duration:0]];
    }
    for (NSMutableArray *row in existingPieces){
        for(SKShapeNode *piece in row){
            if ([currentBlock intersectsNode:piece]) {
                if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) ||
                     (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6))){
                     if(!((fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) || (fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6))) {
                        [currentBlock runAction:[SKAction moveByX:blockWidth y:0 duration:0]];
                        return;
                     }
                }
            }
        }
    }
}

-(void) movePieceRight {
    [currentBlock runAction:[SKAction moveByX:blockWidth y:0 duration:0]];
    if (CGRectGetMaxX((currentBlock.frame)) >= ((tetrisGrid.frame.origin.x * 2)+(blockWidth * 10))) {
        [currentBlock runAction:[SKAction moveByX:-blockWidth y:0 duration:0]];
    }
    for (NSMutableArray *row in existingPieces){
        for(SKShapeNode *piece in row){
            if ([currentBlock intersectsNode:piece]) {
                if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) ||
                     (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6))){
                         if(!((fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) || (fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6))) {
                             [currentBlock runAction:[SKAction moveByX:-blockWidth y:0 duration:0]];
                             return;
                         }
                     }
            }
        }
    }
}

-(void) rotateCounterClockwise {
    
    [currentBlock runAction:[SKAction rotateByAngle:M_PI_2 duration:0]];
    for (NSMutableArray *row in existingPieces){
        for(SKShapeNode *piece in row){
            if ([currentBlock intersectsNode:piece]) {
                if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) ||
                     (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6))){
                         if(!((fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) || (fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6))) {
                             [currentBlock runAction:[SKAction rotateByAngle:-M_PI_2  duration:0]];
                              return;
                              }
                     }
                }
            }
    }

    switch (nextPiece) {
        case YELLOW_SQUARE:
            //do nothing
            break;
        case BLUE_L:
            break;
        case ORANGE_L:
            break;
        case CYAN_STICK:
        {
            CGRect bounds = currentBlock.frame;
            NSLog(@"Bound width: %f", bounds.size.width);
            NSLog(@"Bound height: %f", bounds.size.height);
            
            if (bounds.size.width > bounds.size.height) {
                if(currentBlockOrientation == ZERO){
                    [currentBlock runAction:[SKAction moveByX:-(blockWidth / 2.0) y:(blockHeight / 2.0) duration:0]];
                    NSLog(@"Zero");
                }
                if(currentBlockOrientation == ONE_EIGHTY){
                    [currentBlock runAction:[SKAction moveByX:(blockWidth / 2.0) y:-(blockHeight / 2.0) duration:0]];
                    NSLog(@"One Eighty");
                }
            } else {
                if(currentBlockOrientation == NINETY){
                    [currentBlock runAction:[SKAction moveByX:(blockWidth / 2.0) y:-(blockHeight / 2.0) duration:0]];
                    NSLog(@"Ninety");
                }
                if(currentBlockOrientation == TWO_SEVENTY){
                    [currentBlock runAction:[SKAction moveByX:-(blockWidth / 2.0) y:(blockHeight / 2.0) duration:0]];
                    NSLog(@"Two Seventy");
                }
            }
            break;
        }
        case RED_STAIR:
            break;
        case GREEN_STAIR:
            break;
    }
    currentBlockOrientation = (currentBlockOrientation + 1) % 4;
}

-(void) rotateClockwise {
    
    [currentBlock runAction:[SKAction rotateByAngle:-M_PI_2 duration:0]];
    for (NSMutableArray *row in existingPieces){
        for(SKShapeNode *piece in row){
            if ([currentBlock intersectsNode:piece]) {
                if(!((fabs(CGRectGetMinX(currentBlock.frame) - CGRectGetMaxX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) ||
                     (fabs(CGRectGetMaxX(currentBlock.frame) - CGRectGetMinX(piece.frame)) < 6 &&
                      fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6))){
                         if(!((fabs(CGRectGetMinY(currentBlock.frame) - CGRectGetMaxY(piece.frame)) < 6) || (fabs(CGRectGetMaxY(currentBlock.frame) - CGRectGetMinY(piece.frame)) < 6))) {
                             [currentBlock runAction:[SKAction rotateByAngle:M_PI_2  duration:0]];
                             return;
                         }
                     }
            }
        }
    }
    
    switch (nextPiece) {
        case YELLOW_SQUARE:
            //do nothing
            break;
        case BLUE_L:
            break;
        case ORANGE_L:
            break;
        case CYAN_STICK:
        {
            CGRect bounds = currentBlock.frame;
            NSLog(@"Bound width: %f", bounds.size.width);
            NSLog(@"Bound height: %f", bounds.size.height);
            if (bounds.size.width > bounds.size.height) {
                NSLog(@"If");
                if(currentBlockOrientation == ZERO){
                    [currentBlock runAction:[SKAction moveByX:(blockWidth / 2.0) y:-(blockHeight / 2.0) duration:0]];
                    NSLog(@"Zero");
                }
                if(currentBlockOrientation == ONE_EIGHTY){
                    [currentBlock runAction:[SKAction moveByX:-(blockWidth / 2.0) y:(blockHeight / 2.0) duration:0]];
                    NSLog(@"One Eighty");
                }
            } else {
                if(currentBlockOrientation == NINETY){
                    [currentBlock runAction:[SKAction moveByX:-(blockWidth / 2.0) y:(blockHeight / 2.0) duration:0]];
                    NSLog(@"Ninety");
                }
                if(currentBlockOrientation == TWO_SEVENTY){
                    [currentBlock runAction:[SKAction moveByX:(blockWidth / 2.0) y:-(blockHeight / 2.0) duration:0]];
                    NSLog(@"Two Seventy");
                }
            }
            break;
        }
        case RED_STAIR:
            break;
        case GREEN_STAIR:
            break;
    }
    currentBlockOrientation = (currentBlockOrientation + 1) % 4;
}

- (void) checkIfLineFull {
    
    int counter = 0;
    
    for (int i = 0; i < 20; i++) {
        
        BOOL rowFull = YES;
        
        for (int j = 0; j < 10; j++) {
            
            SKShapeNode *shape = existingPieces[i][j];
            
            if(shape.hidden == YES){
                
                rowFull = NO;
                
            }
            
            else{
                
                counter++;
                
                
                
            }
            
        }
        
        if(rowFull == YES){
            
            NSLog(@"Clearing row: %d",i);
            
            [self clearRow: i];
            
            i--;
            
        }
        
    }
    
    NSLog(@"counter: %d", counter);
    
}







-(void) clearRow: (int)row{
    
    for(int i = row; i < 19; i++){
        
        for(int j = 0; j < 10; j++){
            
            //            SKShapeNode *abovePiece = existingPieces[i+1][j];
            
            //            SKShapeNode *myPiece = existingPieces[i][j];
            
            //            NSLog(@"%@",abovePiece.fillColor);
            
            //            NSLog(@"%@",myPiece.fillColor);
            
            
            
            if (((SKShapeNode *)existingPieces[i + 1][j]).hidden == NO) {
                
                
                
                CGPoint tempNodeOrigin = ((SKShapeNode*)existingPieces[i][j]).position;
                
                [existingPieces[i][j] removeFromParent];
                
                
                
                CGPathRef tempNodePath = ((SKShapeNode*)existingPieces[i + 1][j]).path;
                
                SKShapeNode *tempNode = [SKShapeNode shapeNodeWithPath:tempNodePath];
                
                
                
                tempNode.fillColor = ((SKShapeNode*)existingPieces[i + 1][j]).fillColor;
                
                
                
                tempNode.position = tempNodeOrigin;
                
                
                
                [self addChild:tempNode];
                
                
                
                existingPieces[i][j] = tempNode;
                
            } else {
                
                [existingPieces[i][j] removeFromParent];
                
                existingPieces[i][j] = existingPieces[i + 1][j];
                
            }
            
        }
        
    }
    
    
    
}
@end
