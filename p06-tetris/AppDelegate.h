//
//  AppDelegate.h
//  p06-tetris
//
//  Created by Brian Kim on 3/11/16.
//  Copyright © 2016 bkim35. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

