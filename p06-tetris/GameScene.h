//
//  GameScene.h
//  p06-tetris
//

//  Copyright (c) 2016 bkim35. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
{
    float blockHeight, blockWidth;
    float buttonHeight, buttonWidth;
    NS_ENUM(NSInteger, TetrominoShape) {
        YELLOW_SQUARE = 0,
        BLUE_L = 1,
        ORANGE_L = 2,
        CYAN_STICK = 3,
        RED_STAIR = 4,
        GREEN_STAIR = 5
    };
    
    NS_ENUM(NSInteger, Orientation){
        ZERO = 0,
        NINETY = 1,
        ONE_EIGHTY = 2,
        TWO_SEVENTY = 3
    };
    
    enum Orientation currentBlockOrientation;
    enum TetrominoShape nextPiece;
    UIView *tetrisGrid;
    float gameScreenHeight;
    float gameScreenWidth;
    SKShapeNode *currentBlock;
    int level;
    BOOL pieceBroken;
    
    NSTimer *tetrominoTimer;
    BOOL currentPieceActive;
    NSMutableArray *existingPieces;
}

@end
